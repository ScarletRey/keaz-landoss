$(document).ready(function() {

  $('.smoothscroll').on('click', function (e) {

e.preventDefault();

var target = this.hash,
$target = $(target);

$('html, body').stop().animate({
  'scrollTop': $target.offset().top
}, 500, 'swing');

});

$('.hamburg').on('click', function(){
  $('.hamburg, .nav-list').toggleClass('is-active');
});

new WOW().init();

// Cache selectors
var lastId,
    topMenu = $(".navigation"),
    topMenuHeight = topMenu.outerHeight()+15,
    // All list items
    menuItems = topMenu.find(".smoothscroll"),
    // Anchors corresponding to menu items
    scrollItems = menuItems.map(function(){
      var item = $($(this).attr("href"));
      if (item.length) { return item; }
    });

// Bind to scroll
$(window).scroll(function(){
   // Get container scroll position
   var fromTop = $(this).scrollTop()+topMenuHeight;

   // Get id of current scroll item
   var cur = scrollItems.map(function(){
     if ($(this).offset().top < fromTop)
       return this;
   });
   // Get the id of the current element
   cur = cur[cur.length-1];
   var id = cur && cur.length ? cur[0].id : "";

   if (lastId !== id) {
       lastId = id;
       // Set/remove active class
       menuItems
         .parent().removeClass("current")
         .end().filter("[href='#"+id+"']").parent().addClass("current");
   }
});

});
